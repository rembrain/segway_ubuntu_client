import logging
import requests
import typing as T

from common.exceptions import AuthError, LogoutError, RequestError

logger = logging.getLogger(__name__)


class Connection:
    address = ""
    username = None
    password = None
    headers = None

    RELOGIN_TRIES = 3

    def __init__(self, address, username, password):
        self.address = address
        self.username = username
        self.password = password
        # Add token
        self.login_user()

    def login_user(self):
        data_user = {"username": self.username, "password": self.password}
        res = requests.post(self.address + "/login", json=data_user)
        if res.status_code != 200:
            logger.info(f"r.status_code " + str(res.status_code) + " " + res.text)
            raise AuthError("user/password")

        token = res.json()
        self.headers = {"Authorization": token.get("access_token"), "Refresh": token.get("refresh_token")}

    def _refresh_token(self):
        res = requests.post(self.address + "/refresh", headers=self.headers)
        if res.status_code != 200:
            logger.info(f"r.status_code " + str(res.status_code) + " " + res.text)
            self.login_user()
        else:
            token = res.json()
            self.headers = {"Authorization": token.get("access_token"), "Refresh": token.get("refresh_token")}

    def logout_user(self):
        res = requests.post(self.address + "/logout", headers=self.headers)

        if res.status_code != 200:
            logger.info(f"r.status_code " + str(res.status_code) + " " + res.text)
            raise LogoutError(res.text)

        self.headers = None

    def get_request(self, command, module_name="api", params=None):
        resource = f"{self.address}/{module_name}/{command}"
        fn = requests.get
        return self._perform_request(fn, resource, params=params)

    def post_request(self, command, module_name="api", params=None, body=None):
        resource = f"{self.address}/{module_name}/{command}"
        fn = requests.post
        return self._perform_request(fn, resource, json=body, params=params)

    def put_request(self, command, module_name="api", params=None, body=None):
        resource = f"{self.address}/{module_name}/{command}"
        fn = requests.put
        return self._perform_request(fn, resource, json=body, params=params)

    def delete_request(self, command, module_name="api", params=None, body=None):
        resource = f"{self.address}/{module_name}/{command}"
        fn = requests.delete
        return self._perform_request(fn, resource, params=params)

    def _perform_request(self, request_fn: T.Callable[[str, dict], requests.Response], resource: str, **kwargs):
        """
        Performs a request, returns the resulting JSON response if response code 200
        If we get 403 (Unauthorized), tries to refresh the login then tries to repeat
            the request up to RELOGIN_ATTEMPTS tries
        On all other response codes raise an error

        :param request_fn: function of the requests object that will be executed (GET/POST/DELETE etc.)
        :param resource: resource to which we're making the request
        :param kwargs: kwargs passed directly to request_fn
        Headers are automatically included in the kwargs as long as the class has them
        """
        if not self.headers:
            raise AuthError("Not logged in")

        for i in range(self.RELOGIN_TRIES):
            kwargs["headers"] = self.headers
            res = request_fn(resource, **kwargs)
            if res.status_code == 200:
                return res.json()
            elif res.status_code == 403:
                logger.warning(f"Got unauthorized response while performing request to {resource}, "
                               f"refreshing the token")
                self._refresh_token()
                continue
            else:
                raise RequestError(f"API request to {resource} returned HTTP {res.status_code}")

        raise RequestError(f"Reached maximum amount of retries ({self.RELOGIN_TRIES})")
