import multiprocessing
import time
import json
from rembrain_robot_framework import RobotProcess
import math
import threading
import numpy
import cv2

import common.connection
from common import connection

class Viewer(RobotProcess):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        with open('H.json','r') as f:
            self.H=numpy.array(json.load(f))
        self.width_proj = 500
        self.height_proj = 500

        self.bird_view_left=1340
        self.bird_view_top=5
        self.bird_view_right=1840
        self.bird_view_bottom=995
        self.main_camera_left=320
        self.main_camera_top = 0
        self.main_camera_right=320+640
        self.main_camera_bottom=360

        self.text_x=30
        self.text_y=550

        self.m_per_pixel=0.0066
        self.distance_to_vision=0.6 #m
        self.distance_to_vision_back = 0.5  # m
        self.robot_length=0.9 #m
        self.robot_axis=0.68#m
        self.robot_width=0.6#m
        self.max_curvative = 0.625#1/m
        self.forward=True

        self.cur_curv=None
        self.state=None

        self.cfg = json.loads(connection.get_config())
        self.min_auto_speed=self.cfg['auto_mode']['min_speed']
        self.max_auto_speed = self.cfg['auto_mode']['max_speed']
        self.close_threshold=self.cfg['auto_mode']['close_threshold']

        self.shift_pressed = False
        self.remember = False
        self.state_receiver=threading.Thread(target=self.state_func)
        self.state_receiver.daemon=True
        self.state_receiver.start()


        self.output =None
        self.record=False
        self.z_pressed=False

        self.D=0

        self.task_names=[]
        self.combo=None
        self.update_tasks()

        self.ui_thread = threading.Thread(target=self.ui_func)
        self.ui_thread.daemon = True
        self.ui_thread.start()

    def ui_func(self):
        import tkinter as tk
        from tkinter import ttk
        root = tk.Tk()
        root.title("Buttons")
        label = tk.Label(root, text="Actions")
        label.pack(pady=20)
        # Add a button
        button = tk.Button(root, text="Start remember", command=self.start_remember)
        button.pack(pady=5, padx=20)
        button = tk.Button(root, text="Stop remember", command=self.stop_remember)
        button.pack(pady=5, padx=20)
        button = tk.Button(root, text="Remember", command=self.remember_func)
        button.pack(pady=5, padx=20)
        button = tk.Button(root, text="Open lid", command=self.open_lid)
        button.pack(pady=5, padx=20)
        button = tk.Button(root, text="Close lid", command=self.close_lid)
        button.pack(pady=5, padx=20)

        button = tk.Button(root, text="start record", command=self.start_record)
        button.pack(pady=5, padx=20)
        button = tk.Button(root, text="stop record", command=self.stop_record)
        button.pack(pady=5, padx=20)

        self.combo = ttk.Combobox(
            state="readonly",
            values=self.task_names,
            width=12
        )
        self.combo.place(x=50, y=347)
        label = tk.Label(root, text="Task")
        label.pack(anchor="w",padx=10)

        #self.inputtxt = tk.Text(root,height=1,width=20)
        #self.inputtxt.pack()
        button = tk.Button(root, text="start task", command=self.start_task)
        button.pack(pady=5, padx=20)
        button = tk.Button(root, text="continue task", command=self.continue_task)
        button.pack(pady=5, padx=20)
        # Run the main loop
        root.mainloop()

    def state_func(self):
        from scipy.spatial.transform import Rotation as R
        def is_forward(direction_vector, quaternion):
            forward_vector = R.from_quat(quaternion).apply([1, 0, 0])
            dot_product = numpy.dot(direction_vector, forward_vector)
            return dot_product > 0

        prev_state=None
        self.D=0
        self.state=None
        while True:
            if not self.is_empty('state'):
                self.state=json.loads(self.consume('state'))
                if not prev_state is None:
                    dp = numpy.array([self.state['odom']['position']['x'] - prev_state['odom']['position']['x'],
                                      self.state['odom']['position']['y'] - prev_state['odom']['position']['y'], 0])
                    q = numpy.array(
                        [self.state['odom']['orientation']['x'], self.state['odom']['orientation']['y'], self.state['odom']['orientation']['z'],
                         self.state['odom']['orientation']['w']])
                    if is_forward(dp, q):
                        d= numpy.linalg.norm(dp)
                    else:
                        d = -numpy.linalg.norm(dp)


                    if int(self.D)!=int(self.D+d):
                        #remember each meter if requested
                        if self.remember:
                            print('remember')
                            self.publish(json.dumps(
                                {'op': 'remember', 'source': 'viewer'}),
                                         'viewer_to_robot')
                    self.D+=d
                prev_state=self.state
            time.sleep(0.01)
    def start_remember(self):
        self.remember=True
        self.D=0
        self.publish(json.dumps({'op': 'start_remember', 'source': 'viewer'}), 'viewer_to_robot')
    def stop_remember(self):
        self.remember=False
        self.publish(json.dumps({'op': 'stop_remember', 'source': 'viewer'}), 'viewer_to_robot')
    def start_record(self):
        self.record=True
        fourcc = cv2.VideoWriter_fourcc(*'XVID')
        self.output = cv2.VideoWriter('output.mp4', fourcc, 10, (640, 360))
    def stop_record(self):
        if not self.output is None:
            self.output.release()
        self.record=False
    def start_task(self):
        index=self.combo.current()
        if index>=0 and index<len(self.task_names):
            text=self.task_names[index]+":rembrain@rembrain.ai"
        task=common.connection.get_task(text)

        self.cfg = json.loads(connection.get_config())
        self.cfg['task_name'] = text
        connection.set_config(self.cfg)

        self.task=task['task']
        self.auto_mode=True
        self.publish(json.dumps({'op': 'set_traj', "session":self.task['session'], 'source': 'viewer'}), 'viewer_to_robot')
        self.publish(json.dumps({'op': 'trigger', 'value':'start_auto', 'source': 'viewer'}), 'viewer_to_robot')


    def continue_task(self):
        self.auto_mode=True
        self.publish(json.dumps({'op': 'trigger', 'value':'start_auto', 'source': 'viewer'}), 'viewer_to_robot')

    def open_lid(self):
        self.publish(json.dumps({'op': 'open_lid', 'source': 'viewer'}), 'viewer_to_robot')
    def close_lid(self):
        self.publish(json.dumps({'op': 'close_lid', 'source': 'viewer'}), 'viewer_to_robot')
    def remember_func(self):
        print('remember')
        self.publish(json.dumps({'op': 'remember', 'source': 'viewer'}),'viewer_to_robot')

    def update_tasks(self):
        try:
            tasks = connection.get_tasks('rembrain@rembrain.ai')
            index = 0
            for i in range(len(tasks)):
                task = tasks[i]
                if 'type' in task['task'].keys() and task['task']['type'] == 'segway':
                    #self.task_names.append(task['task'])
                    self.task_names.append(task['name'])
                    index += 1

        except Exception as e:
            print(e)
        if not self.combo is None:
            self.combo['values']=self.task_names
        return

    def run(self):
        self.log.info(f"{self.__class__.__name__} started, name: {self.name}.")
        import pygame.camera
        import pygame.image
        import pygame.draw as draw
        import pygame.mouse as mouse
        import pygame
        global  pygame
        import sys

        pygame.init()

        screen = pygame.display.set_mode((1920,1000))
        pygame.display.set_caption("Viewer")
        pyimage=None
        pyimage_h=None
        pyimage_left=None
        pyimage_right = None
        pyimage_ml=None
        pyimage_back = None
        pyimage_h_back = None
        prev_pressed = False
        prev_curve=None
        self.closest_person_dist=None
        self.ml_curve=None
        self.last_ml_curve = time.time()
        self.ml_curve_follow = None
        self.ml_feet_pos = None
        self.last_feet=time.time()
        a_pressed_prev = False
        self.auto_mode=False
        self.track_length=None
        self.total_length=None
        self.prev_track_length=None
        self.task=None
        self.prev_state='stopped'


        s=time.time()

        while True:
            for e in pygame.event.get():
                if e.type == pygame.QUIT:
                    sys.exit(0)

            crossing = False
            if not self.state is None:
                if self.state['state'] == 'crossing_road':
                    crossing = True

            screen.fill((0,0,0))
            if not self.state is None and 'battery' in self.state and not self.state['battery'] is None:
                font = pygame.font.Font('freesansbold.ttf', 32)
                ss='battery: '+str(self.state['battery'])+ ' D: '+f"{self.D:.{1}f}"
                if not self.ml_curve is None:
                    ss+=" curve "+'%.2f' % self.ml_curve
                if 'state' in self.state:
                    ss+=' '+self.state['state']
                text = font.render(ss, True, (0, 255, 0), (0,0,128))
                textRect = text.get_rect()
                textRect.topleft=(self.text_x,self.text_y)
                screen.blit(text,textRect)


            if not self.is_empty(consume_queue_name="ml"):
                ml_event=json.loads(self.consume(queue_name='ml'))
                if ml_event['type']=='track':
                    if not ml_event['value']['curve'] is None:

                        self.track_length=ml_event['value']['track_length']
                        self.ml_curve=ml_event['value']['curve']
                        self.area_close=ml_event['value']['free_areas'][0]
                        self.area_middle=ml_event['value']['free_areas'][1]
                        self.area_far = ml_event['value']['free_areas'][2]
                        self.total_length=ml_event['value']['length']
                        self.last_ml_curve=time.time()
                    else:
                        if not self.ml_curve is None:
                            self.publish(json.dumps({'op': 'set_move', 'speed': 0.0, 'curvature': 0,
                                                     'source': 'viewer'}), 'viewer_to_robot_move')
                        self.ml_curve =None
                if ml_event['type']=='feet_detector':
                    self.ml_curve_follow=ml_event['value']['curve']
                    self.ml_feet_pos = ml_event['value']['screen_pos']
                    self.last_feet=time.time()

                if ml_event['type']=='people_detector':
                    poses = ml_event['value']['poses']
                    min=float('+inf')
                    was=False
                    for p in poses:
                        if p[1]>0:
                            if abs(p[0]/p[1])<0.5:
                                if p[1]<min:
                                    min=p[1]
                                    was=True
                    if was:
                        self.closest_person_dist=min
                    else:
                        self.closest_person_dist = None
                    self.last_feet=time.time()

            if time.time()-self.last_ml_curve>1.0:
                self.ml_curve=None
            if time.time()-self.last_feet>1.0:
                self.ml_curve_follow=None
                self.ml_feet_pos=None

            # draw frame
            if not self.is_empty(consume_queue_name="video_to_viewer"):
                #draw original image
                #if not hasattr(self,'prev'):
                #    self.prev=time.time()
                #print(time.time()-self.prev)

                self.prev=time.time()

                image = self.consume(queue_name="video_to_viewer")[0]
                pyimage=pygame.image.frombuffer(image.tostring(), image.shape[1::-1], "BGR")

                #draw homography
                k = 2
                if crossing:
                    k = 4
                image_scaled=cv2.resize(image,(image.shape[1]*k,image.shape[0]*k))
                image_h = cv2.warpPerspective(image_scaled, self.H, (self.width_proj, self.height_proj))
                pyimage_h = pygame.image.frombuffer(image_h.tostring(), image_h.shape[1::-1], "BGR")
            if not self.is_empty(consume_queue_name="video_to_viewer_ml_debug"):
                #draw original image
                image = self.consume(queue_name="video_to_viewer_ml_debug")[0]
                if self.record and not self.output is None:
                    self.output.write(image)
                pyimage_ml=pygame.image.frombuffer(image.tostring(), image.shape[1::-1], "BGR")
            if not self.is_empty(consume_queue_name="video_to_viewer_left"):
                image = self.consume(queue_name="video_to_viewer_left")[0]
                pyimage_left = pygame.image.frombuffer(image.tostring(), image.shape[1::-1], "BGR")
            if not self.is_empty(consume_queue_name="video_to_viewer_right"):
                image = self.consume(queue_name="video_to_viewer_right")[0]
                pyimage_right = pygame.image.frombuffer(image.tostring(), image.shape[1::-1], "BGR")
                
            if not self.is_empty(consume_queue_name="video_to_viewer_internal"):
                image = self.consume(queue_name="video_to_viewer_internal")[0]
                pyimage_internal = pygame.image.frombuffer(image.tostring(), image.shape[1::-1], "BGR")

            if not self.is_empty(consume_queue_name="video_to_viewer_back"):
                #draw original image
                image = self.consume(queue_name="video_to_viewer_back")[0]
                pyimage_back=pygame.image.frombuffer(image.tostring(), image.shape[1::-1], "BGR")

                #draw homography

                image_scaled=cv2.resize(image,(image.shape[1]*4,image.shape[0]*4))
                image_h = cv2.warpPerspective(image_scaled, self.H, (self.width_proj, self.height_proj))
                image_h=cv2.rotate(image_h, cv2.ROTATE_180)
                pyimage_h_back = pygame.image.frombuffer(image_h.tostring(), image_h.shape[1::-1], "BGR")


            if not pyimage_h is None:
                screen.blit(pyimage_h, (self.bird_view_left, self.bird_view_top))


            if crossing:
                if not pyimage_left is None:
                    screen.blit(pyimage_left, (0, 160))
                if not pyimage_right is None:
                    screen.blit(pyimage_right, (640, 160))
                if not pyimage is None:
                    screen.blit(pyimage, (320+160, 0))
                if not pyimage_back is None:
                    screen.blit(pyimage_back, (160+320, 360+80))
            else:
                if not pyimage is None:
                    screen.blit(pyimage, (320, 0))
                if not pyimage_left is None:
                    screen.blit(pyimage_left, (0, 90))
                if not pyimage_right is None:
                    screen.blit(pyimage_right, (640+320, 90))
                if not pyimage_back is None:
                    screen.blit(pyimage_back, (160+320, 360))
                    
            if not pyimage_left is None:
                screen.blit(pyimage_internal, (640, 620))
                    
            if not pyimage_h_back is None:
                shift=(self.distance_to_vision+self.distance_to_vision_back+self.robot_length) / self.m_per_pixel
                screen.blit(pyimage_h_back, (self.bird_view_left, self.bird_view_top+(500+shift)))
            if not pyimage_ml is None:
                screen.blit(pyimage_ml, (0, 620))

            w = self.robot_width / self.m_per_pixel
            h = self.robot_length / self.m_per_pixel
            shift = self.distance_to_vision / self.m_per_pixel
            draw.rect(screen, (255, 0, 0), (int(self.bird_view_left + 250 - w / 2),
                                            int(self.bird_view_top + 500 + shift),
                                            int(w), int(h)),
                      width=3)

            self.draw_curvative(mouse.get_pos(), screen)

            pygame.display.flip()
            # grab next frame

            #control

            if time.time()-s>0.2:

                if not self.state is None:
                    if (self.prev_state!=self.state['state']) or \
                            (self.state['state'] == 'auto_mode' and self.task is None):
                        #load task
                        if not (self.prev_state=='idle' and self.state['state']=='stopped') and \
                            not (self.state['state']=='crossing_road'):
                            self.cfg=json.loads(connection.get_config())
                            if 'task_name' in self.cfg.keys():
                                
                                task = common.connection.get_task(self.cfg['task_name'])
                                if not task is None:
                                    route=task['task']
                                    segment_name=route['segments'][0]['name']+':'+route['segments'][0]['author']
                                    task = common.connection.get_task(segment_name)
                                    self.task = task['task']

                    if self.state['state'] == 'auto_mode' and not self.task is None: # and self.auto_mode==True:
                        self.tick_auto()
                        if not self.prev_track_length is None and not self.track_length is None:
                            print(self.prev_track_length,self.track_length,self.task['checkpoints'])
                            for i in range(len(self.task['checkpoints'])):
                                dist=self.task['checkpoints'][i]
                                if self.prev_track_length-0.00001<dist and dist<self.track_length+0.00001:
                                    self.prev_track_length=None
                                    self.prev_track=None
                                    self.publish(json.dumps({'op': 'set_move', 'speed': 0.0, 'curvature': 0,
                                                             'source': 'viewer'}), 'viewer_to_robot_move')
                                    self.publish(json.dumps({'op': 'trigger', 'value': 'cross_road'}),
                                                 'viewer_to_robot_move')
                                    self.auto_mode=False
                                    break
                        if not self.track_length is None and not self.total_length is None:
                            if self.track_length>self.total_length-1.8:
                                self.publish(json.dumps({'op': 'trigger', 'value': 'stop'}),
                                             'viewer_to_robot_move')
                                self.task=None

                        if not self.track_length is None:
                            self.prev_track_length = self.track_length



                    self.prev_state=self.state['state']

                keys = pygame.key.get_pressed()
                self.shift_pressed=keys[pygame.K_LSHIFT]
                if keys[pygame.K_z] and not self.z_pressed:
                    self.publish(json.dumps({'op': 'trigger', 'value': 'cross_road','source':'viewer'}), 'viewer_to_robot')
                self.z_pressed=keys[pygame.K_z]


                if keys[pygame.K_SPACE]:
                    self.task=None
                    self.publish(json.dumps({'op': 'trigger', 'value':'stop'}),
                                 'viewer_to_robot_move')
                #go automated
                if keys[pygame.K_a]:
                    self.tick_auto()
                    a_pressed_prev=True
                else:
                    if a_pressed_prev:
                        a_pressed_prev=False
                        self.publish(json.dumps({'op': 'set_move', 'speed': 0.0, 'curvature': 0,
                                                 'source': 'viewer'}),'viewer_to_robot_move')

                pressed=False
                speed_A=1.0
                if mouse.get_pressed()[0]:
                    pressed=True
                if mouse.get_pressed()[2]:
                    pressed=True
                    speed_A=2.0

                if self.shift_pressed:
                    speed_A=3.0


                if not self.cur_curv is None:
                    if self.forward:
                        speed=0.5
                    else:
                        speed=-0.2
                    if pressed:
                        self.publish(
                            json.dumps({'op': 'set_move', 'speed': speed*speed_A, 'curvature': -self.cur_curv, 'source': 'viewer'}),
                            'viewer_to_robot_move')
                    else:
                        if prev_pressed and not pressed:
                            self.publish(json.dumps({'op': 'set_move', 'speed': 0, 'curvature': -self.cur_curv, 'source': 'viewer'}),
                                 'viewer_to_robot_move')
                    prev_curve=self.cur_curv
                else:
                    if not prev_curve is None:
                        self.publish(
                            json.dumps({'op': 'set_move', 'speed': 0, 'curvature': 0, 'source': 'viewer'}),
                            'viewer_to_robot_move')
                        prev_curve=None



                prev_pressed=pressed

                s=time.time()


    def tick_auto(self):
        if not self.ml_curve is None:
            speed = self.min_auto_speed + (self.max_auto_speed - self.min_auto_speed) * (
                        self.area_far + self.area_middle) / 2.0
            print('speed ', speed, self.max_auto_speed, self.area_close, self.area_far)
            if self.area_close < self.close_threshold:
                speed = 0
                print('stop')

            # check people around
            k_person = 1.0
            if not self.closest_person_dist is None:
                min_dist = 2.0
                max_dist = 4.0
                if self.closest_person_dist < min_dist:
                    k_person = 0.0
                else:
                    if self.closest_person_dist < max_dist:
                        k_person = (self.closest_person_dist - min_dist) / (max_dist - min_dist)
                    else:
                        k_person = 1.0
            speed = speed * k_person

            k_curve=1.0
            if abs(self.ml_curve)>0.5:
                k_curve=0.5
            else:
                k_curve=1.0-abs(self.ml_curve)
            speed=speed*k_curve

            self.publish(json.dumps({'op': 'set_move', 'speed': speed, 'curvature': -self.ml_curve,
                                     'source': 'viewer'}),
                         'viewer_to_robot_move')
    def consume_queues(self):
        return super().consume_queues

    def draw_forward(self,screen,curve):
        pixels = []
        pixels_left_back_screen=[]
        pixels_left_back = []
        pixels_right_back = []
        pixels_right_back_screen = []

        pixels_main=[]
        h = 500
        XC = (self.bird_view_left + self.bird_view_right) / 2.0
        YC = (self.bird_view_top + 500) + (self.distance_to_vision + self.robot_axis) / self.m_per_pixel
        H_inv=numpy.linalg.inv(self.H)

        for yi in range(int(h)):
            if curve != 0:
                r = abs(1.0 / curve)
            else:
                r = 10000

            y = yi * self.m_per_pixel
            yc=0


            if r * r - y * y >= 0:
                x = -math.sqrt(r * r - y * y) + r

                alpha = math.pi-math.asin(y / r)
                r_left_back=r+self.robot_width/2.0
                r_right_back = r - self.robot_width / 2.0
                xc = r
                if curve < 0:
                    x = -x
                    xc=-xc
                    alpha=math.pi-alpha
                    r_left_back-=self.robot_width
                    r_right_back+= self.robot_width



                if abs(x) < (self.bird_view_right - self.bird_view_left) / 2.0 * self.m_per_pixel:

                    pixels.append((x / self.m_per_pixel + XC, -y / self.m_per_pixel + YC))
                    p=numpy.array([x / self.m_per_pixel + XC-self.bird_view_left,
                                   -y / self.m_per_pixel + YC-self.bird_view_top,1])
                    p=numpy.dot(H_inv,p)
                    x_screen=p[0]/p[2]/2.0
                    y_screen = p[1] / p[2]/2.0
                    if x_screen>=0 and y_screen>=0 and x_screen<640 and y_screen<360:
                        pixels_main.append([x_screen+self.main_camera_left,y_screen+self.main_camera_top])

                x_left_back = math.cos(alpha) * r_left_back + xc
                y_left_back = math.sin(alpha) * r_left_back + yc
                x=x_left_back / self.m_per_pixel + XC
                y=-y_left_back / self.m_per_pixel + YC
                if x>=self.bird_view_left and x<self.bird_view_right and y>=self.bird_view_top and y<self.bird_view_bottom:
                    pixels_left_back.append([x,y])
                    p = numpy.array([x -self.bird_view_left,y-self.bird_view_top, 1])
                    p = numpy.dot(H_inv, p)
                    x_screen = p[0] / p[2] / 2.0
                    y_screen = p[1] / p[2] / 2.0
                    if x_screen >= 0 and y_screen >= 0 and x_screen < 640 and y_screen < 360:
                        pixels_left_back_screen.append([x_screen + self.main_camera_left, y_screen + self.main_camera_top])

                x_right_back = math.cos(alpha) * r_right_back + xc
                y_right_back = math.sin(alpha) * r_right_back + yc
                x = x_right_back / self.m_per_pixel + XC
                y = -y_right_back / self.m_per_pixel + YC
                if x >= self.bird_view_left and x < self.bird_view_right and y >= self.bird_view_top and y < self.bird_view_bottom:
                    pixels_right_back.append([x, y])
                    p = numpy.array([x -self.bird_view_left,y-self.bird_view_top, 1])
                    p = numpy.dot(H_inv, p)
                    x_screen = p[0] / p[2] / 2.0
                    y_screen = p[1] / p[2] / 2.0
                    if x_screen >= 0 and y_screen >= 0 and x_screen < 640 and y_screen < 360:
                        pixels_right_back_screen.append(
                            [x_screen + self.main_camera_left, y_screen + self.main_camera_top])

        if len(pixels)>=2:
            pygame.draw.lines(screen, (0, 0, 255), False, pixels, width=2)
        if len(pixels_left_back)>=2:
            pygame.draw.lines(screen, (0, 0, 128), False, pixels_left_back, width=2)
        if len(pixels_right_back)>=2:
            pygame.draw.lines(screen, (0, 0, 128), False, pixels_right_back, width=2)
        if len(pixels_main)>=2:
            pygame.draw.lines(screen, (0, 0, 255), False, pixels_main, width=2)
        if len(pixels_left_back_screen)>=2:
            pygame.draw.lines(screen, (0, 0, 128), False, pixels_left_back_screen, width=2)
        if len(pixels_right_back_screen)>=2:
            pygame.draw.lines(screen, (0, 0, 128), False, pixels_right_back_screen, width=2)

    def draw_curvative(self,mouse_pos,screen):
        def circle_curvative(x, y):
            return (2*x)/(x ** 2 + y ** 2)

        curv_to_save=None
        if mouse_pos[0]>self.bird_view_left and mouse_pos[0]<self.bird_view_right and \
            mouse_pos[1]>self.bird_view_top and mouse_pos[1]<self.bird_view_bottom:

            XC=(self.bird_view_left+self.bird_view_right)/2.0
            YC=(self.bird_view_top+500)+(self.distance_to_vision+self.robot_axis)/self.m_per_pixel

            x=(mouse_pos[0]-XC)*self.m_per_pixel
            y=(-mouse_pos[1]+YC)*self.m_per_pixel

            if y>self.robot_length/2.0:
                curvative=circle_curvative(x,y)
                if curvative>self.max_curvative:
                    curvative=self.max_curvative
                if curvative<-self.max_curvative:
                    curvative=-self.max_curvative

                curv_to_save=curvative

                self.draw_forward(screen,curvative)
                self.forward=True

            if y<-self.robot_length/2.0:
                curvative = circle_curvative(x, y)
                print(curvative)
                if curvative > self.max_curvative:
                    curvative = self.max_curvative
                if curvative < -self.max_curvative:
                    curvative = -self.max_curvative
                curv_to_save = curvative

                h = 500

                pixels = []
                for yi in range(int(h)):
                    if curvative != 0:
                        r = abs(1.0 / curvative)
                    else:
                        r = 10000

                    y = yi * self.m_per_pixel
                    if r * r - y * y >= 0:
                        x = -math.sqrt(r * r - y * y) + r
                        if curvative < 0:
                            x = -x
                        if abs(x) < (self.bird_view_right - self.bird_view_left) / 2.0 * self.m_per_pixel:
                            pixels.append((x / self.m_per_pixel + XC, y / self.m_per_pixel + YC))

                pygame.draw.lines(screen, (0, 0, 255), False, pixels, width=2)
                self.forward=False

        if mouse_pos[0] > self.main_camera_left and mouse_pos[0] < self.main_camera_right and \
                mouse_pos[1] > self.main_camera_top and mouse_pos[1] < self.main_camera_bottom:
            x_screen=mouse_pos[0]-self.main_camera_left
            y_screen = mouse_pos[1] - self.main_camera_top
            if y_screen<220:
                y_screen=220
            p=numpy.array([x_screen*2,y_screen*2,1])
            p=numpy.dot(self.H,p)
            x_bird=p[0]/p[2]
            y_bird=p[1]/p[2]

            XC = 250
            YC = 500 + (self.distance_to_vision + self.robot_axis) / self.m_per_pixel

            x = (x_bird - XC) * self.m_per_pixel
            y = (-y_bird + YC) * self.m_per_pixel

            curvative = circle_curvative(x, y)
            if curvative > self.max_curvative:
                curvative = self.max_curvative
            if curvative < -self.max_curvative:
                curvative = -self.max_curvative

            curv_to_save = curvative
            self.draw_forward(screen, curvative)
            self.forward = True

        self.cur_curv=curv_to_save

