import multiprocessing
import time
import json
from rembrain_robot_framework import RobotProcess
import math
import threading
import numpy
import cv2
import sys

class DetectorProcessor(RobotProcess):
    def __init__(self, *args, **kwargs):
        sys.path.insert(0, './YOLOX_deepsort_tracker')
        sys.path.insert(0, './YOLOX_deepsort_tracker/YOLOX')

        from tracker_utils.tracker import Tracker
        global Tracker

        super().__init__(*args, **kwargs)

    def run(self):
        self.log.info(f"{self.__class__.__name__} started, name: {self.name}.")

        tracker = Tracker(ckpt='yolox_s.pth',filter_class=['person'])

        count=0
        start=time.time()
        while True:
            time.sleep(0.005)
            if not self.is_empty(consume_queue_name="video_to_ml"):
                image = self.consume(queue_name="video_to_ml")[0]
                s=time.time()
                img_visual, bbox = tracker.update(image)

                count+=1
                if count==100:
                    print('FPS ',100/(time.time()-start),' frame.shape ',image.shape)
                    start=time.time()
                    count=0

