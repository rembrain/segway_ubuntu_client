import multiprocessing
import time
import json
from rembrain_robot_framework import RobotProcess
import math
import threading
import numpy
import cv2

class GestureProcessor(RobotProcess):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
    
    def run(self):
        self.log.info(f"{self.__class__.__name__} started, name: {self.name}.")


        while True:
            time.sleep(0.005)
            if not self.is_empty(consume_queue_name="video_to_ml"):
                image = self.consume(queue_name="video_to_ml")[0]
                print('image.shape ',image.shape)

