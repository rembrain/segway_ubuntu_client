call .venv/Scripts/activate.bat

echo Environment activated

set REGISTER_ADDRESS=https://ADDRESS
set ROBOT_NAME=ROBOT_NAME
set RRF_USERNAME=USER_NAME
set RRF_PASSWORD=USER_PASSWORD
set WEBSOCKET_GATE_URL=wss://ADDRESS/wssocket/

echo Running main.py
python3 main.py