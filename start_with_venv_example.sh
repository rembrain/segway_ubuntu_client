#!/bin/bash
source venv/bin/activate
export REGISTER_ADDRESS="https://ADDRESS"
export ROBOT_NAME="ROBOTNAME"
export RRF_USERNAME="USERNAME"
export RRF_PASSWORD="PASSWORD"
export WEBSOCKET_GATE_URL="wss://ADDRESS/wssocket/"

python3 main.py